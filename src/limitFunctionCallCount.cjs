const limitFunctionCallCount = (cb, n) => {
  let callCount = 0;
  return function (...args) {
    if (callCount < n) {
      callCount++;
      return cb(...args);
    }
    return null;
  };
};

module.exports = limitFunctionCallCount;
