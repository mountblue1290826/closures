const counterFactory = () => {
  let counter = 0;
  const obj = {
    increment: () => {
      counter++;
      return counter;
    },
    decrement: () => {
      counter--;
      return counter;
    },
  };
  return obj;
};

module.exports = counterFactory;
