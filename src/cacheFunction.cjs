const cacheFunction = (cb) => {
  const cache = {};
  return function (...args) {
    const key = JSON.stringify(args);
    if (cache[key]) {
      return cache[key];
    }
    const result = cb(...args);
    cache[key] = result;
    return result;
  };
};

module.exports = cacheFunction;
