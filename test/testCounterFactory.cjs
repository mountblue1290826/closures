const counterFactory = require("../src/counterFactory.cjs");

let counter = counterFactory();

console.log(counter.increment());
console.log(counter.increment());
console.log(counter.increment());

console.log(counter.decrement());
console.log(counter.decrement());
console.log(counter.decrement());
