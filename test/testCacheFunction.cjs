const cacheFunction = require("../src/cacheFunction.cjs");

const add = (a,b) =>{
    return a+b;
}

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(2,2));
console.log(cachedAdd(2,2));


console.log(cachedAdd(2,4));
console.log(cachedAdd(2,4));