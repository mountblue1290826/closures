const testLimitFunctionCallCount = require("../src/limitFunctionCallCount.cjs");

const add = (a, b) => {
  return a + b;
};

const limitedAdd = testLimitFunctionCallCount(add, 2);

console.log(limitedAdd(1, 2));
console.log(limitedAdd(1, 2));
console.log(limitedAdd(1, 2));
